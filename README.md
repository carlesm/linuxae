# Instalación
Ejecutar el playbook con una versión reciente de Ansible sobre Debian 9 o 10. En el caso de Debian 9, hay que instalar desde los repositorios de Ansible una versión más reciente que la que trae.

# Configuración previa
Copiar linuxaeparam.yml.dist a linuxaeparam.yml y editar al menos el nombre de usuario.

# Uso
    ansible-playbook makemelinuxae.yml

# Certificado DNIe
Instalar a mano /usr/lib/libpkcs11-fnmtdnie.so

# Más instrucciones
[LinuxAE v5](https://linuxae.org.es/)



